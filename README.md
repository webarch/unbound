# Webarchitects Unbound Ansible Role

An Ansible role to install, configure, enable and start [Unbound](https://nlnetlabs.nl/projects/unbound/about/) on Debian and Ubuntu.

## Variables

See the [defaults/main.yml](defaults/main.yml) file for the default variables, the [vars/main.yml](vars/main.yml) file for the preset variables, the [meta/argument_specs.yml](meta/argument_specs.yml) file for the variable argument specification for the default variables and [meta/internal_argument_specs.yml](internal_argument_specs.yml) file for the internal variable argument specification.

### unbound

The tasks in the role will be skipped if `unbound` is `false`, it defaults to `false`, set it to `true` for the tasks in this role to be run.

### unbound_conf

A list of dictionaries of Unbound configuration files, each list element is a dictionary consisting of `path`, for the file path, `state` for the file state, `present` or `absent` and `conf` for a YAML dictionary of configuration that the file will contain, these files are templated using the [templates/unbound.conf.j2](templates/unbound.conf.j2) template and backup files are created when files are changed.

The one exception to this is the the `/etc/unbound/unbound.conf.d/forward-addrs.conf` file, this is generated from the [templates/forward-addrs.conf.j2](templates/forward-addrs.conf.j2) template, see [below for further details](#unbound-configuration).

Note that this role requires that all `/etc/unbound/unbound.conf.d/*.conf` files are defined to ensure that there isn't any configuration present that is not managed by this Ansible role.

### unbound_forward_addrs

An optional list for `forward-addr`, when this list is defined and not an empty list a `/etc/unbound/unbound.conf.d/forward-addrs.conf` file is generated from [a template](templates/forward-addrs.conf.j2), see [below for further details](#unbound-configuration).

### unbound_openresolv_conf

An optional dictionary of configuration that will be used to update to `/etc/resolvconf.conf` when the `openresolv` package is present, it is installed by default, remove it from the `unbound_pkgs` list if you want to use another method to manage `/etc/resolv.conf`, for example `systemd-resolved`.

### unbound_pkgs

A list of dictionaries of Linux distros and the packages that should be present and absent for each distro.

### unbound_resolvconf

A boolean, `unbound_resolvconf` defaults to false and this results in the `unbound-resolvconf` service being stopped and disabled, set `unbound_resolvconf` to true if you want to use this service to manage `/etc/resolv.conf`, note that `openresolv` should be added to the list of packages that should be absent if you use this service.

### unbound_verify

A boolean, `unbound_verify` defaults to `true`, which results in the variables being checked with the [meta/argument_specs.yml](meta/argument_specs.yml) and the [meta/internal_argument_specs.yml](meta/internal_argument_specs.yml), set `unbound_verify` to `false` to skip these checks.

## Unbound configuration

The Unbound configuration files are YAML-like to the extent that most configuration can be written and templated as YAML and this role takes advantage of that, `unbound_conf` is a list of paths, state and configuration, for example the Debian Bookworm default configuration can be represented as:

```yaml
unbound_conf:
  - path: /etc/unbound/unbound.conf
    state: present
    conf:
      "include-toplevel": /etc/unbound/unbound.conf.d/*.conf
  - path: /etc/unbound/unbound.conf.d/remote-control.conf
    state: present
    conf:
      "remote-control":
        "control-enable": "yes"
        "control-interface": /run/unbound.ctl
  - path: /etc/unbound/unbound.conf.d/root-auto-trust-anchor-file.conf
    state: present
    conf:
      server:
        "auto-trust-anchor-file": /var/lib/unbound/root.key
```

However Unbound allows configuation keys and values to be repeated whereas in YAML this is not allowed, this can be got around for keys due to the way in which Unbound merges configuration files in the Debian default directory, `/etc/unbound/unbound.conf.d/*.conf`, for example in order to repeat the `interface` key the following can be used:

```yaml
unbound_conf:
  - path: /etc/unbound/unbound.conf.d/interface_ipv4.conf
    state: present
    conf:
      server:
        "interface": 0.0.0.0
  - path: /etc/unbound/unbound.conf.d/interface_ipv6.conf
    state: present
    conf:
      server:
        "interface": ::0
```

One thing this doesn't work for is for the [forward-zone](https://unbound.docs.nlnetlabs.nl/en/latest/manpages/unbound.conf.html#forward-zone-options) for example to forward all requests to Cloundflare, Google and Quad9 configuration like this can be used:

```yaml
forward-zone:
  name: "."
  forward-ssl-upstream: "yes"
  forward-addr: "1.1.1.1@853#one.one.one.one"
  forward-addr: "8.8.4.4@853#dns.google"
  forward-addr: "8.8.8.8@853#dns.google"
  forward-addr: "9.9.9.9@853#dns.quad9.net"
```

However the above is not valid YAML and the `name: "."` can't be repeated in multiple files, so this role has a `unbound_forward_addrs` variable that takes an optional list of `forwardd-addr`'s, for example:

```yaml
unbound_forward_addrs:
  - 1.1.1.1@853#one.one.one.one
  - 8.8.4.4@853#dns.google
  - 8.8.8.8@853#dns.google
  - 9.9.9.9@853#dns.quad9.net
```

And templates these to `/etc/unbound/unbound.conf.d/forward-addrs.conf`, this generate the configuration above, all other configuration is represented by YAML dictionaries.

## Local DNS (Stub) Resolver for a Single Machine

The defaults are suitable for a local machine and are in part based on the the [Unbound Local DNS (Stub) Resolver for a Single Machine documentation](https://unbound.docs.nlnetlabs.nl/en/latest/use-cases/local-stub.html) but use the `openserolv` package rather than the `systemd-resolved`, to use `systemd-resolved` comment the `openserolv` package from `unbound_pkgs` and use a `systemd-resolved` configuration with the [systemd role](https://git.coop/webarch/systemd):

```yaml
systemd_units:
  - name: systemd-resolved
    files:
      - path: /etc/systemd/resolved.conf.d/resolved.conf
        conf:
          Resolve:
            DNS: 127.0.0.1
            DNSSEC: "yes"
            DNSStubListener: "no"
    pkgs:
      - systemd-resolved
```

## Copyright

Copyright 2024-2025 Chris Croome, &lt;[chris@webarchitects.co.uk](mailto:chris@webarchitects.co.uk)&gt;.

This role is released under [the same terms as Ansible itself](https://github.com/ansible/ansible/blob/devel/COPYING), the [GNU GPLv3](LICENSE).
